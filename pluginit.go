package pluginit

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
	"xu.com/go_project/logger"
	"xu.com/project/utils/httpUtils"
)

var (
	configUrl = "http://47.93.14.116:17001/config"
	db        *gorm.DB
	redisPool *redis.Pool
)

func initLogger(config map[string]interface{}) error {
	logger.Init(config)
	return nil
}

func GetRedisPool() *redis.Pool {
	return redisPool
}

func initDial(config map[string]interface{}) (conn redis.Conn, e error) {
	return redis.Dial("tcp", "47.93.14.116"+":"+"6379",
		//redis.DialPassword(config.Password),
		redis.DialConnectTimeout(time.Second*time.Duration(30)),
		redis.DialReadTimeout(time.Second*time.Duration(30)),
		redis.DialWriteTimeout(time.Second*time.Duration(30)))
}

func initRedis(config map[string]interface{}) error {
	redisPool = &redis.Pool{
		Dial: func() (conn redis.Conn, err error) {
			conn, err = initDial(config)
			if err != nil {
				fmt.Println("init redis failed:", err)
				panic(err)
			}
			return
		},
		//TestOnBorrow:nil,
		TestOnBorrow: func(c redis.Conn, t time.Time) (err error) {
			if time.Since(t) < time.Minute {
				return
			}
			_, err = c.Do("PING")
			if err != nil {
				fmt.Println("redis connection failed", err)
				panic(err)
			}
			return
		},

		MaxIdle:         3000,
		MaxActive:       3000,
		IdleTimeout:     time.Second * time.Duration(30),
		MaxConnLifetime: time.Second * time.Duration(3600),
		Wait:            true,
	}

	return nil
}

func GetGormDb() *gorm.DB {
	return db
}

func initMysql(config map[string]interface{}) error {
	var err error
	db, err = gorm.Open("mysql", "root:123456@tcp(47.93.14.116:3306)/db_zny?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println("init mysql error:", err)
		return err
	}
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)
	db.LogMode(true)

	return nil
}

func Init(name string) {
	fmt.Println("init plugin start")

	response, err := httpUtils.Get(configUrl, "name="+name)
	if err != nil {
		fmt.Println("init plugin request config error:", err)
		panic(err)
	}

	var responseMap map[string]interface{}
	err = json.Unmarshal([]byte(response), &responseMap)
	if err != nil {
		fmt.Println("init plugin json.Unmarshal config error:", err)
		panic(err)
	}
	if responseMap["code"].(string) != "200" {
		panic(errors.New("init plugin responseMap code error"))
	}

	configMap := responseMap["data"].(map[string]interface{})

	if configMap["mysql"] != nil {
		mysqlConfig := configMap["mysql"].(map[string]interface{})
		err = initRedis(mysqlConfig)
	}

	if configMap["redis"] != nil {
		redisConfig := configMap["redis"].(map[string]interface{})
		err = initMysql(redisConfig)
	}

	if configMap["logger"] != nil {
		loggerConfig := configMap["logger"].(map[string]interface{})
		err = initLogger(loggerConfig)
	}

	if err != nil {
		fmt.Println("init plugin error:", err)
		panic(err)
	}

	fmt.Println("init plugin end")
}
